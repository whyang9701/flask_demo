from app import app
from flask import render_template,send_from_directory

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/<path:path>')
def sendPage(path):
    return render_template((path+'.html'))


@app.route('/images/<path:path>')
def send_img(path):
    return send_from_directory('templates/images', path)

@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('templates/css', path)

